import React, { Component } from 'react';

import Header from './Components/Header';
import Content from './Components/Content';
import Footer from "./Components/Footer";

import './App.css';


class App extends Component {

  constructor(props) {
      super(props);
      this.appConfig = {
          headerText: 'header',
          footerText: '© All rights reserved.',
          users: [
              {id: 1, name: 'Sam', age: 33},
              {id: 2, name: 'Pete', age: 22},
              {id: 3, name: 'David', age: 44},
              {id: 4, name: 'Ashley', age: 22},
              {id: 5,  name: 'Nancy', age: 33},
              {id: 6, name: 'Vivien', age: 44}
          ]
      };

  }


  render() {
    return (
      <div className="user-list">

      </div>
    );
  }
}

export default App;
